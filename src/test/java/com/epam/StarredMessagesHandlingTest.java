package com.epam;

import com.epam.bo.AccountDeletingBO;
import com.epam.bo.GmailLoginBO;
import com.epam.bo.StarredMessagesBO;
import com.epam.listener.TestListener;
import com.epam.utils.DriverProvider;
import com.epam.utils.Property;
import org.testng.annotations.*;

import static org.testng.Assert.assertTrue;

@Listeners({TestListener.class})
public class StarredMessagesHandlingTest {
    private static final int NUMBER_OF_MESSAGE_TO_HANDLE = 3;

    @DataProvider
    private Object[][] users() {
        return new Object[][]{{Property.DRIVER.getProperty("user.email"), Property.DRIVER.getProperty("user.password")}};
    }

    @Test(dataProvider = "users")
    private void testImportantMessageHandling(String email, String password) {
        GmailLoginBO logInBO = new GmailLoginBO();
        logInBO.login(email, password);
        assertTrue(logInBO.wasAccountSuccessfullyAdded(email), "new account wasn't successfully added");
        logInBO.goToPrimaryPage();
        StarredMessagesBO starredMessagesBO = new StarredMessagesBO();
        starredMessagesBO.markMessagesAsStarred(NUMBER_OF_MESSAGE_TO_HANDLE);
        assertTrue(starredMessagesBO.areMessagesMarkedAsStarred(NUMBER_OF_MESSAGE_TO_HANDLE), "messages wasn't marked as starred");
        starredMessagesBO.deleteStarredMessages(NUMBER_OF_MESSAGE_TO_HANDLE);
        assertTrue(starredMessagesBO.areMessagesDeleted(NUMBER_OF_MESSAGE_TO_HANDLE), "messages wasn't removed");
    }

    @AfterMethod
    private void tearDown() {
        AccountDeletingBO accountDeletingBO = new AccountDeletingBO();
        accountDeletingBO.deleteLastAddedAccount();
        DriverProvider.closeDriver();
    }
}
