package com.epam.bo;

import com.epam.po.impl.GmailLoginPage;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GmailLoginBO {
    private static final Logger LOGGER = LogManager.getLogger();
    private GmailLoginPage loginPage;

    public GmailLoginBO() {
        loginPage = new GmailLoginPage();
    }

    @Step("Gmail login step")
    public void login(String email, String password) {
        LOGGER.info("logging in Gmail");
        loginPage.tapGotItButton();
        loginPage.tapAddAnotherAccountButton();
        loginPage.tapGoogleButton();
        loginPage.fillEmailField(email);
        loginPage.tapEmailNextButton();
        loginPage.fillPasswordField(password);
        loginPage.tapPasswordNextButton();
        loginPage.tapAgreeButton();
    }

    @Step("Verifying if new account has been added")
    public boolean wasAccountSuccessfullyAdded(String email) {
        return loginPage.ifAccountLabelContains(email);
    }

    @Step("Going to 'Primary' messages page")
    public void goToPrimaryPage() {
        LOGGER.info("Going to 'Primary' messages page");
        loginPage.tapTakeMeToGmailButton();
    }
}