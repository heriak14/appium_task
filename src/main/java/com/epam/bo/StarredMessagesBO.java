package com.epam.bo;

import com.epam.model.Email;
import com.epam.po.impl.IncomingMessagesPage;
import com.epam.po.impl.StarredMessagesPage;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class StarredMessagesBO {
    private static final Logger LOGGER = LogManager.getLogger();
    private IncomingMessagesPage incomingMessagesPage;
    private StarredMessagesPage starredMessagesPage;
    private int messagesBeforeDeleting;
    private int messagesAfterDeleting;

    public StarredMessagesBO() {
        incomingMessagesPage = new IncomingMessagesPage();
        starredMessagesPage = new StarredMessagesPage();
    }

    @Step("Marking messages as starred step")
    public void markMessagesAsStarred(int number) {
        LOGGER.info("marking " + number + " messages as starred");
        incomingMessagesPage.tapStarCheckBoxes(number);
    }

    @Step("Checking if messages has been marked as starred")
    public boolean areMessagesMarkedAsStarred(int number) {
        LOGGER.info("checking if " + number + " messages are marked as starred");
        List<Email> incomingMessages = incomingMessagesPage.getEmails(number);
        LOGGER.info("Marked messages: " + incomingMessages + "\n");
        switchToStarredMessagesFolder();
        List<Email> starredMessages = starredMessagesPage.getEmails(number);
        LOGGER.info("Starred messages: " + starredMessages + "\n");
        return starredMessages.containsAll(incomingMessages);
    }

    @Step("Switching to 'Starred' folder step")
    private void switchToStarredMessagesFolder() {
        LOGGER.info("switching to starred messages folder");
        incomingMessagesPage.tapMenuButton();
        incomingMessagesPage.tapStarredMessagesButton();
    }

    @Step("Deleting starred messages step")
    public void deleteStarredMessages(int number) {
        LOGGER.info("deleting " + number + " starred messages");
        messagesBeforeDeleting = starredMessagesPage.getMessagesNumber();
        starredMessagesPage.pressMessages(number);
        starredMessagesPage.tapDeleteButton();
        messagesAfterDeleting = starredMessagesPage.getMessagesNumber();
    }

    @Step("Verifying that messages has been deleted")
    public boolean areMessagesDeleted(int number) {
        LOGGER.info("Checking if " + number + " messages have been deleted");
        return (messagesBeforeDeleting - messagesAfterDeleting) == number;
    }
}