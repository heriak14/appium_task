package com.epam.bo;

import com.epam.po.impl.DeviceAccountsManagerPage;
import com.epam.po.impl.GmailAccountsPage;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AccountDeletingBO {
    private static final Logger LOGGER = LogManager.getLogger();
    private GmailAccountsPage accountsPage;
    private DeviceAccountsManagerPage accountsManagerPage;

    public AccountDeletingBO() {
        accountsPage = new GmailAccountsPage();
        accountsManagerPage = new DeviceAccountsManagerPage();
    }

    @Step("Deleting last added account step")
    public void deleteLastAddedAccount() {
        DeviceAccountsManagerPage deviceAccountsManagerPage = new DeviceAccountsManagerPage();

        deviceAccountsManagerPage
                .tapGoogleAccountsButton()
                .tapConfirmationButton();

        LOGGER.info("Delete last added account");
        accountsPage.tapAccountButton();
        accountsPage.tapManageAccountsButton();
        accountsManagerPage.tapGoogleAccountsButton();
        accountsManagerPage.tapLastAddedAccount();
        accountsManagerPage.tapMoreButton();
        accountsManagerPage.tapRemoveAccountButton();
        accountsManagerPage.tapConfirmationButton();
    }
}
