package com.epam.model;

import java.util.Objects;

public class Email {
    private String sender;
    private String subject;
    private String date;

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Email)) return false;
        Email email = (Email) o;
        return sender.equals(email.sender) &&
                subject.equals(email.subject) &&
                date.equals(email.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sender, subject, date);
    }

    @Override
    public String toString() {
        return "Email{" +
                "sender='" + sender + '\'' +
                ", subject='" + subject + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
