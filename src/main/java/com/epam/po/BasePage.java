package com.epam.po;

import com.epam.utils.DriverProvider;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
    protected static final Logger LOGGER = LogManager.getLogger();
    protected AndroidDriver<MobileElement> driver;

    public BasePage() {
        driver = DriverProvider.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
}
