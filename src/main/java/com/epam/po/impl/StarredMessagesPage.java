package com.epam.po.impl;

import com.epam.model.Email;
import com.epam.po.BasePage;
import com.epam.utils.EmailParser;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;

import java.util.List;
import java.util.stream.Collectors;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static java.time.Duration.ofSeconds;

public class StarredMessagesPage extends BasePage {
    private static final int PRESSING_TIME = 1;
    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup")
    private List<MobileElement> messages;
    @AndroidFindBy(id = "com.google.android.gm:id/delete")
    private MobileElement deleteButton;

    public List<Email> getEmails(int number) {
        LOGGER.info("parsing " + number + " messages to model");
        return messages.stream()
                .limit(number)
                .map(EmailParser::parseEmailFromMobileElement)
                .collect(Collectors.toList());
    }

    public int getMessagesNumber() {
        LOGGER.info("getting number of messages on 'Starred' folder");
        return messages.size();
    }

    public void pressMessages(int messagesNumber) {
        LOGGER.info("pressing " + messagesNumber + " messages");
        messages.stream()
                .limit(messagesNumber)
                .forEach(this::pressElement);
    }

    private void pressElement(MobileElement element) {
        new TouchAction(driver)
                .press(element(element))
                .waitAction(waitOptions(ofSeconds(PRESSING_TIME)))
                .release()
                .perform();
    }

    public void tapDeleteButton() {
        LOGGER.info("tapping 'Delete' button");
        deleteButton.click();
    }
}
