package com.epam.po.impl;

import com.epam.po.BasePage;
import com.epam.utils.Waiter;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class DeviceAccountsManagerPage extends BasePage {
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[5]/android.widget.LinearLayout[2]")
    private MobileElement googleAccountsButton;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[last()]/android.widget.RelativeLayout")
    private MobileElement lastAddedAccount;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.view.View")
    private MobileElement moreButton;
    @AndroidFindBy(xpath = "//android.widget.ListView/android.widget.LinearLayout[8]/android.widget.RelativeLayout")
    private MobileElement removeAccountButton;
    @AndroidFindBy(id = "android:id/button1")
    private MobileElement confirmationButton;

    public DeviceAccountsManagerPage tapGoogleAccountsButton() {
        LOGGER.info("tapping 'Google' button");
        googleAccountsButton.click();
        return  this;
    }

    public DeviceAccountsManagerPage tapLastAddedAccount() {
        LOGGER.info("tapping last added account");
        lastAddedAccount.click();
        return  this;

    }

    public void tapMoreButton() {
        LOGGER.info("tapping 'More' button");
        moreButton.click();
    }

    public void tapRemoveAccountButton() {
        LOGGER.info("tapping 'Remove account' button");
        removeAccountButton.click();
    }

    public void tapConfirmationButton() {
        LOGGER.info("tapping confirmation button");
        confirmationButton.click();
        Waiter.visibilityOf(moreButton);
    }
}
