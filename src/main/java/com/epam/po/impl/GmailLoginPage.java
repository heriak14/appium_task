package com.epam.po.impl;

import com.epam.po.BasePage;
import com.epam.utils.Waiter;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;

public class GmailLoginPage extends BasePage {
    @AndroidFindBy(id = "com.google.android.gm:id/welcome_tour_got_it")
    private MobileElement gotItButton;
    @AndroidFindBy(id = "com.google.android.gm:id/setup_addresses_add_another")
    private MobileElement addAnotherAccountButton;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]")
    private MobileElement googleButton;
    @AndroidFindBy(xpath = "//android.widget.EditText")
    private MobileElement emailField;
    @AndroidFindBy(xpath = "//android.view.View[4]/android.widget.Button")
    private MobileElement emailNextButton;
    @AndroidFindBy(xpath = "//android.view.View[1]/android.widget.EditText")
    private MobileElement passwordField;
    @AndroidFindBy(xpath = "//android.view.View[4]/android.widget.Button")
    private MobileElement passwordNextButton;
    @AndroidFindBy(xpath = "//android.view.View[4]/android.widget.Button")
    private MobileElement agreeButton;
    @AndroidFindBy(id = "com.google.android.gm:id/action_done")
    private MobileElement takeMeToGmailButton;

    public void tapGotItButton() {
        LOGGER.info("tapping 'GOT IT' button");
        gotItButton.click();
    }

    public void tapAddAnotherAccountButton() {
        LOGGER.info("tapping 'Add another account' button");
        addAnotherAccountButton.click();
    }

    public void tapGoogleButton() {
        LOGGER.info("tapping 'Google' button");
        googleButton.click();
    }

    public void fillEmailField(String email) {
        LOGGER.info("sending keys to email field");
        emailField.sendKeys(email);
    }

    public void tapEmailNextButton() {
        LOGGER.info("tapping 'Next' button");
        emailNextButton.click();
    }

    public void fillPasswordField(String password) {
        LOGGER.info("sending keys to password field");
        passwordField.sendKeys(password);
    }

    public void tapPasswordNextButton() {
        LOGGER.info("tapping 'Next' button");
        passwordNextButton.click();
    }

    public void tapAgreeButton() {
        LOGGER.info("tapping 'Agree' button");
        agreeButton.click();
    }

    public boolean ifAccountLabelContains(String email) {
        LOGGER.info("checking if accounts list contains account with email " + email);
        try {
            Waiter.visibilityOf(driver.findElement(By.xpath("//android.widget.TextView[@text='" + email + "']")));
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }

    public void tapTakeMeToGmailButton() {
        LOGGER.info("tapping 'Take me to Gmail' button");
        takeMeToGmailButton.click();
    }
}
