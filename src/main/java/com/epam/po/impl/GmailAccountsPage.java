package com.epam.po.impl;

import com.epam.po.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class GmailAccountsPage extends BasePage {
    @AndroidFindBy(id = "com.google.android.gm:id/og_apd_ring_view")
    private MobileElement accountButton;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[2]/android.widget.TextView")
    private MobileElement manageAccountsButton;


    public void tapAccountButton() {
        LOGGER.info("tapping account button in top right corner");
        accountButton.click();
    }

    public void tapManageAccountsButton() {
        LOGGER.info("tapping 'Manage accounts ion this device' button");
        manageAccountsButton.click();
    }
}
