package com.epam.po.impl;

import com.epam.model.Email;
import com.epam.po.BasePage;
import com.epam.utils.EmailParser;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class IncomingMessagesPage extends BasePage {
    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.view.ViewGroup")
    private List<MobileElement> messages;
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Open navigation drawer']")
    private MobileElement menuButton;
    @AndroidFindBy(xpath = "//androidx.drawerlayout.widget.DrawerLayout//android.view.ViewGroup[1]/android.widget.TextView[1]")
    private MobileElement starredMessagesButton;

    public void tapStarCheckBoxes(int starsNumber) {
        LOGGER.info("tapping messages star checkboxes");
        messages.stream()
                .limit(starsNumber)
                .map(e -> e.findElement(By.xpath(".//android.widget.ImageView[2]")))
                .forEach(MobileElement::click);
    }

    public List<Email> getEmails(int number) {
        LOGGER.info("parsing page messages to model");
        return messages.stream()
                .limit(number)
                .map(EmailParser::parseEmailFromMobileElement)
                .collect(Collectors.toList());
    }

    public void tapMenuButton() {
        LOGGER.info("tapping menu button");
        menuButton.click();
    }

    public void tapStarredMessagesButton() {
        LOGGER.info("tapping 'Starred' button");
        starredMessagesButton.click();
    }
}
