package com.epam.utils;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class DriverProvider {
    private static final int DEFAULT_IMPLICIT_WAIT = Integer.parseInt(Property.DRIVER
            .getProperty("wait.implicit.default"));
    private static ThreadLocal<AndroidDriver<MobileElement>> DRIVER_PULL = new ThreadLocal<>();

    private DriverProvider() {
    }

    public static AndroidDriver<MobileElement> getDriver() {
        if (Objects.isNull(DRIVER_PULL.get())) {
            initDriver();
        }
        return DRIVER_PULL.get();
    }

    private static void initDriver() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("deviceName", Property.DRIVER.getProperty("device.name"));
        caps.setCapability("udid", Property.DRIVER.getProperty("device.udid"));
        caps.setCapability("platformName", Property.DRIVER.getProperty("platform.name"));
        caps.setCapability("platformVersion", Property.DRIVER.getProperty("platform.version"));
        caps.setCapability("appPackage", Property.DRIVER.getProperty("app.package"));
        caps.setCapability("appActivity",Property.DRIVER.getProperty("app.activity"));
        try {
            DRIVER_PULL.set(new AndroidDriver<>(new URL(Property.DRIVER.getProperty("appium.server.url")),caps));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        DRIVER_PULL.get().manage().timeouts().implicitlyWait(DEFAULT_IMPLICIT_WAIT, TimeUnit.SECONDS);
    }

    public static void closeDriver() {
        DRIVER_PULL.get().closeApp();
        DRIVER_PULL.set(null);
    }
}
