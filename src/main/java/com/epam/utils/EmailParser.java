package com.epam.utils;

import com.epam.model.Email;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

public class EmailParser {
    private static final String SENDER_LOCATOR = ".//android.widget.TextView[1]";
    private static final String SUBJECT_LOCATOR = ".//android.widget.TextView[3]";
    private static final String DATE_LOCATOR = ".//android.widget.TextView[2]";

    public static Email parseEmailFromMobileElement(MobileElement element) {
        Email email = new Email();
        email.setSender(element.findElement(By.xpath(SENDER_LOCATOR)).getText());
        email.setSubject(element.findElement(By.xpath(SUBJECT_LOCATOR)).getText());
        email.setDate(element.findElement(By.xpath(DATE_LOCATOR)).getText());
        return email;
    }
}
