package com.epam.utils;

import io.appium.java_client.MobileElement;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {
    private static final int DEFAULT_WAITING_TIME = Integer.parseInt(Property.DRIVER.getProperty("wait.explicit.default"));

    public static void elementToBeClickable(MobileElement element) {
        new WebDriverWait(DriverProvider.getDriver(), DEFAULT_WAITING_TIME).ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void visibilityOf(MobileElement element) {
        new WebDriverWait(DriverProvider.getDriver(), DEFAULT_WAITING_TIME).until(ExpectedConditions.visibilityOfAllElements(element));
    }
}
